\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{5}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Team}{5}{subsection.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Extended Team}{7}{subsection.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.1}Professors}{7}{subsubsection.1.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.2}Supporters}{7}{subsubsection.1.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Client}{8}{subsection.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.3.1}SSI Schäfer IT Solutions GmbH}{8}{subsubsection.1.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Project Management}{9}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Waterfall Model}{9}{subsection.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Milestones}{12}{subsection.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Requirements}{13}{subsection.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Work Breakdown Structure}{15}{subsection.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Work Packages and RASCI Matrix}{16}{subsection.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6}Planning Poker}{18}{subsection.2.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7}Sequence Diagrams}{19}{subsection.2.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.8}Communication}{21}{subsection.2.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.8.1}Internal: HeySpace}{21}{subsubsection.2.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.8.2}External: E-Mail}{21}{subsubsection.2.8.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Technologies}{22}{section.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Android}{22}{subsection.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}Java}{22}{subsubsection.3.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2}XML}{23}{subsubsection.3.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.3}SQLite}{24}{subsubsection.3.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.4}Room Persistence Library}{24}{subsubsection.3.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.5}Material Design}{25}{subsubsection.3.1.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Version Control}{26}{subsection.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1}Git}{26}{subsubsection.3.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2}Atlassian BitBucket}{27}{subsubsection.3.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.3}Atlassian SourceTree}{27}{subsubsection.3.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}LaTeX}{28}{subsection.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.1}TexMaker}{28}{subsubsection.3.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.2}TeX Live}{28}{subsubsection.3.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}MPAndroidChart}{29}{subsection.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5}Firebase}{29}{subsection.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6}Tools}{30}{subsection.3.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.6.1}Android Studio}{30}{subsubsection.3.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.6.2}Moqups.com}{30}{subsubsection.3.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.6.3}Postman API}{31}{subsubsection.3.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.6.4}MindView 7.0}{31}{subsubsection.3.6.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.6.5}Microsoft Excel}{32}{subsubsection.3.6.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.6.6}SequenceDiagram.org}{32}{subsubsection.3.6.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Project Design}{33}{section.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Mockups}{33}{subsection.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}How design works in Android Studio}{34}{subsection.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1}Android Studio Layout Editor}{34}{subsubsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2}Density-Independent-Pixels}{36}{subsubsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.3}Layouts used in our application}{37}{subsubsection.4.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}SlideMenu}{38}{subsection.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1}menu\_user.xml}{39}{subsubsection.4.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.2}menu\_list.xml}{39}{subsubsection.4.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.3}Method: onBackPressed()}{40}{subsubsection.4.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.4}Method: onNavigationItemSelected(MenuItem item)}{41}{subsubsection.4.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.5}Method: overridePendingTransition(int enterAnim, int exitAnim)}{41}{subsubsection.4.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}Login Dialog}{42}{subsection.4.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.4.1}login\_menu.xml}{42}{subsubsection.4.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.4.2}Method: setOnEditorActionListener()}{43}{subsubsection.4.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.4.3}Method: setOnCheckedChangeListener()}{44}{subsubsection.4.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.4.4}Method: loadSharedPreferences()}{44}{subsubsection.4.4.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.4.5}Method: startLogin()}{45}{subsubsection.4.4.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5}Home Screen}{46}{subsection.4.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.1}The design of the home screen}{46}{subsubsection.4.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.2}Starting the time recording}{47}{subsubsection.4.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.3}Stopping the time recording}{47}{subsubsection.4.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.4}Timer}{48}{subsubsection.4.5.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.5}Multilevel Dropdown Menu}{49}{subsubsection.4.5.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.6}Using the home menu as a guest}{50}{subsubsection.4.5.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.6}Worktime List}{51}{subsection.4.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.6.1}The design of the worktime list}{52}{subsubsection.4.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.6.2}WorktimelistAdapter}{54}{subsubsection.4.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.6.3}Dialogs}{55}{subsubsection.4.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.6.4}Edit Function}{56}{subsubsection.4.6.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.6.5}Synchronize, Filter and Add}{56}{subsubsection.4.6.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.6.6}Using the Worktime List as a guest}{57}{subsubsection.4.6.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.7}Project List}{58}{subsection.4.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.7.1}RecyclerView}{58}{subsubsection.4.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.7.2}ProjectActivity and ProjectListAdapter}{59}{subsubsection.4.7.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.7.3}Method: showColors()}{60}{subsubsection.4.7.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.7.4}ColorPicker}{61}{subsubsection.4.7.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.8}Statistics}{62}{subsection.4.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.8.1}The design of the statistics}{62}{subsubsection.4.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.8.2}StatisticsTabAdapter}{63}{subsubsection.4.8.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.9}Export}{64}{subsection.4.9}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.9.1}E-Mail Validation}{64}{subsubsection.4.9.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.9.2}Method: openDatePicker(TextInputEditText etTime)}{65}{subsubsection.4.9.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.10}Settings}{66}{subsection.4.10}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.10.1}Basic information}{66}{subsubsection.4.10.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.10.2}Auto-Login}{66}{subsubsection.4.10.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.10.3}Method: checkNotificationsOn()}{67}{subsubsection.4.10.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.10.4}Method: showPopupMenu()}{68}{subsubsection.4.10.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Project Webservices}{70}{section.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Descriptions}{70}{subsection.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.1}Jira Software}{70}{subsubsection.5.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.2}Worktime Server}{71}{subsubsection.5.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.3}Retrofit}{72}{subsubsection.5.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Basic Structure}{74}{subsection.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.1}Webservice Model}{74}{subsubsection.5.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.2}Delegate Interface}{76}{subsubsection.5.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.3}Background Processes}{77}{subsubsection.5.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.4}RetroFit Request Interface}{79}{subsubsection.5.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}Requests}{82}{subsection.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.1}Login Worktime}{82}{subsubsection.5.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.2}CheckToken}{84}{subsubsection.5.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.3}GetWorktime}{86}{subsubsection.5.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.4}PostWorktime}{89}{subsubsection.5.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.5}DeleteWorktime}{92}{subsubsection.5.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.6}GetIssueInfo}{94}{subsubsection.5.3.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.7}Login Jira}{95}{subsubsection.5.3.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.8}GetIssues}{99}{subsubsection.5.3.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.3.9}Logout Jira}{102}{subsubsection.5.3.9}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Project Database}{104}{section.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Room Architecture}{104}{subsection.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.1.1}Database}{105}{subsubsection.6.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.1.2}Database Access Object}{105}{subsubsection.6.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.1.3}Entity}{107}{subsubsection.6.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Usage}{108}{subsection.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.1}Read}{108}{subsubsection.6.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.2}Delete}{109}{subsubsection.6.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.3}Save}{110}{subsubsection.6.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.4}Update}{112}{subsubsection.6.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Project App Functionalities}{114}{section.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1}Widget}{114}{subsection.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.1}Requirements}{114}{subsubsection.7.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.2}View Layout}{115}{subsubsection.7.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.3}AppWidgetProviderInfo}{116}{subsubsection.7.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.4}Widget Configuration Activity}{117}{subsubsection.7.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.5}AppWidgetProvider}{119}{subsubsection.7.1.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.6}BroadcastReceiver}{126}{subsubsection.7.1.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2}Statistics}{127}{subsection.7.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.1}Statistics Weekly}{127}{subsubsection.7.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.2}Statistics Monthly}{130}{subsubsection.7.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.3}Statistics Yearly}{130}{subsubsection.7.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.3}Export}{131}{subsection.7.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.1}Method onExportTime()}{131}{subsubsection.7.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.2}Method createCSVFile()}{133}{subsubsection.7.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.4}Worktime Listing}{134}{subsection.7.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.4.1}Filtering Worktimes}{135}{subsubsection.7.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5}Project/Issue Listing}{138}{subsection.7.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.6}Notifications}{140}{subsection.7.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.6.1}The setup of Firebase}{140}{subsubsection.7.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.6.2}Scheduling the notification}{141}{subsubsection.7.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.6.3}Handling the notification}{143}{subsubsection.7.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Conclusion}{145}{section.8}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1}Kerstin Klaminger}{145}{subsection.8.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.2}Matthäus Schwarzkogler}{145}{subsection.8.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.3}Bernhard Heiß}{146}{subsection.8.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.4}Matthias Pöschl}{146}{subsection.8.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.5}Mario Schweiger}{146}{subsection.8.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Appendix}{147}{section.9}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.1}Minutes of Meetings}{147}{subsection.9.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.2}Time Sheet}{149}{subsection.9.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.3}List of Figures}{150}{subsection.9.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.4}List of Tables}{152}{subsection.9.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.5}Code Listing}{153}{subsection.9.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.6}List of Abbreviations}{156}{subsection.9.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.7}Bibliography}{157}{subsection.9.7}%
