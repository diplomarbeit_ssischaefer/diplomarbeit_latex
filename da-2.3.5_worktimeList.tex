%Worktime

\renewcommand{\familydefault}{\sfdefault}
\Author{\daAuthorFour}

\subsection{Worktime List}
Depending on the filter that is set in the \code{WorktimeActivity}, the scrollable list contains and displays all the filtered \code{Worktime} objects. On the bottom right you can find three buttons (from left to right):
\begin{itemize}
\item Synchronize
\vspace{-2mm}
\item Filter
\vspace{-2mm}
\item Add
\end{itemize}

\begin{figure}[H]
\includegraphics[width=0.3\textwidth]{images/worktime_list.jpg}
\caption{Worktime List}
\end{figure}
\pagebreak
\subsubsection{The design of the worktime list}
In comparison to the home menu, the worktime list is a rather complex design consisting of two .xml files.
\paragraph*{worktime\_list.xml}
This is the main .xml file. The main component is again a  \code{ConstraintLayout}, which contains and aligns all the necessary design elements. These are:
\begin{itemize}
\item \code{SwipeRefreshLayout}\\
To be able to refresh the list, a so called \code{SwipeRefreshLayout} is needed. This makes it possible to swipe the list down and trigger an event that creates a \linebreak\code{Runnable()} instance which creates a loading icon and waits 2 seconds until all the data is loaded into the list.
\end{itemize}
\begin{lstlisting}[caption={onRefreshListener() for the SwipeRefreshLayout}]
refreshLayout.setOnRefreshListener(
  new SwipeRefreshLayout.OnRefreshListener(){
    @Override
    public void onRefresh(){
      (new Handler()).postDelayed(new Runnable(){
        @Override
        public void run(){
          if(isSyncFinished()){
            Log.e("Refresh", "Working");
            loadData();
          }else{
            Log.e("Refresh", "Not Working");
          }
          refreshLayout.setRefreshing(false);
        }
      }, 2000);
    }
  }
);
\end{lstlisting}
\begin{itemize}
\vspace{-5mm}
\item \code{RecyclerView}
\\Inside the \code{SwipeRefreshLayout}, the component that contains all the list items can be found. Together with the \code{WorktimelistAdapter} the \\\code{RecyclerView} handles, formats and displays each item in the list.
\vspace{-2mm}
\item \code{FloatingActionButton}
\\Three of these are to be found on the bottom right corner. As the name suggests, these buttons are elevated which makes them look like as if they are floating above the list. 
\end{itemize}
\paragraph*{worktime\_list\_item.xml}
In this file, the design of each item is defined. \\
A \code{RelativeLayout} was used as the parent component. It is very similar to a \\\code{ConstraintLayout}, except it is a little bit older and therefore slower. But the main reason it was used was because of its ability to overlap components which is not possible with a \code{ConstraintLayout}.\\
Inside of the \code{RelativeLayout}, there are two \code{CardView} elements. A \code{CardView} component elevates all child objects and their surroundings and appears to look like a floating card.

\begin{itemize}
\item \textbf{cv\_time}\\
This is the \code{CardView} that displays the duration as well as the start and end time. A \code{LinearLayout}, which can be used to align elements directly below each other or side by side, was used to bring the two \code{TextView} elements that display the above mentioned into position.
\vspace{-2mm}
\item \textbf{cv\_overview}\\
This \code{Cardview} shows the date, the issuekey and in case the project is synchronized, it will show a blue checkmark at the top right. Here, a \code{ConstraintLayout} was needed again, because this time, there are two \code{TextView} elements, which could have been aligned a lot easier with an \code{LinearLayout}, but because we also have a \code{CheckBox} that had to be positioned in the top right corner, a \code{ConstraintLayout} was necessary.
\end{itemize}
\pagebreak
\subsubsection{WorktimelistAdapter}
The class \code{WorktimelistAdapter} extends \code{RecyclerView.Adapter}, which makes it able to bind data to views, that can later be displayed in the \code{RecyclerView}.
\begin{lstlisting}[caption={RecyclerView.Adapter}]
public class WorktimelistAdapter extends RecyclerView.Adapter<WorktimelistAdapter.ViewHolder>
\end{lstlisting}
The most important method in this class is \code{onBindViewHolder()}. This method has two parameters, which are:
\begin{itemize}
\item A \code{ViewHolder} variable that describes an item view, which represents one entry in the list of \code{Worktime} objects.
\vspace{-2mm}
\item An \code{Integer} is used to get the right object out of a list of the type \\\code{LinkedList<Worktime>}. This object will then be added onto the \code{ViewHolder} which will display it in the \code{RecyclerView}.
\end{itemize}
Beforehand, a \code{ViewHolder} class has to be created, so that the \code{RecyclerView} knows which variable it has to bind to which view. This is done via an inner class.
\begin{lstlisting}[caption={ViewHolder}]
public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
  ...
  ViewHolder(View itemView){
    super(itemView);
    textView = itemView.findViewById(R.id.tv_date);
    cv = itemView.findViewById(R.id.cv_time);
    tvFromTo = itemView.findViewById(R.id.tv_fromTo);
    tvDuration = itemView.findViewById(R.id.tv_duration);
    tvDate = itemView.findViewById(R.id.tv_date);
    tvProjectid = itemView.findViewById(R.id.tv_projectid);
    cbSynced = itemView.findViewById(R.id.cb_synced);
    itemView.setOnClickListener(this);

    cv.bringToFront();
    tvFromTo.bringToFront();
    tvDuration.bringToFront();
  }
}
\end{lstlisting}

Also, inside this class, a \code{onClickListener()} had to be set for the edit function. This will be described more in depth on page \pageref{sec:edit} in the section "\ref{sec:edit} Edit Function". It was only necessary to create a interface called \code{ItemClickListener} with a method called \code{onItemClick()}, which was then overwritten in the \code{WorktimeActivity}.
\begin{lstlisting}[caption={ItemCLickListener}]
public interface ItemClickListener{void onItemClick(View view, int position);}
\end{lstlisting}

\subsubsection{Dialogs}

A dialog is a small window that prompts the user to make a decision or enter additional information. A dialog does not fill the screen and is normally used for modal events that require users to take an action before they can proceed \parencite{dialogs}. \\
\\
In our case, dialogs where used for editing, filtering and adding \code{Worktime} objects. To guarantee the readability of the code, we created a new class for each dialog.
\paragraph*{Basic setup of a Dialog}

There are different types of dialogs. We decided to use an \code{AlertDialog}, because a \code{AlertDialog} can show a custom created layout, so we were able to create our own .xml layout file and design it.
\\  
To display such a dialog, four different objects are needed:
\begin{itemize}
\item \code{Dialog}\\
This is the base class, which will store all the properties.
\vspace{-2mm}
\item \code{AlertDialog.Builder}\\
This object has to call its own method \code{create(View view)}, which will return an \code{AlertDialog} object and store it in the \code{Dialog} object.
\vspace{-2mm}
\item \code{LayoutInflater}\\
Creates and saves a \code{View} object from the given .xml file. 
\vspace{-2mm}
\item \code{View}\\
This object represents the area on the screen where the dialog will be shown. It will be used to create the \code{AlertDialog}.
\end{itemize}
At last, only the method \code{Dialog.show()} has to be called to be able to see the dialog.
\\
\begin{lstlisting}[caption={Dialog}]
AlertDialog.Builder builder = new AlertDialog.Builder(this);
LayoutInflater inflater = LayoutInflater.from(this);
View editDialogView = inflater.inflate(R.layout.edit_worktime_dialog, null);
builder.setView(editDialogView);
Dialog dialog = builder.create();
dialog.show();
\end{lstlisting}


\subsubsection{Edit Function}\label{sec:edit}

In the overwritten \code{onItemClick()} method of the \code{WorktimeActivity} we only had to call the method \code{showEditDialog(int position)} and provide the position of the clicked item, so that the values of the selected item can be shown in the dialog. This makes it possible to create and show a dialog when a recognized click on one of the list items is performed.

\begin{figure}[H]
\includegraphics[width=0.3\textwidth]{images/edit_dialog.jpg}
\caption{Edit Dialog}
\end{figure}

In this dialog you can change all values of the \code{Worktime} object such as start time, end time, issuekey and description. After pressing the confirm button, a new \code{Worktime} object will be created and replaced with the old one.

\subsubsection{Synchronize, Filter and Add}
Three \code{FloatingActionButton} elements, each one representing one function, can be found on the button right corner of the \code{WorktimeActivity}. These three represent the synchronize, filter and add function.
\paragraph*{Synchronize}\\
When pressing the synchronize button, the method \code{syncWorktimes()}, in which the \code{Webservice} instance will synchronize all unsynchronized \code{Worktime} objects, will be called. The icon of the synchronize button will rotate animatedly by 180 degree, to signalize that the synchronization is finished.\\
\begin{lstlisting}[caption={Synchronize}]
FloatingActionButton fabSync = findViewById(R.id.fab_sync);
float deg = fabSync.getRotation() + 180F;
fabSync.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
\end{lstlisting}
\paragraph*{Filter}\\
Via a click on the filter button, a dialog will pop up. Here the user can filter by:
\begin{itemize}
\item start and end time
\vspace{-2mm}
\item issuekey
\vspace{-2mm}
\item synchronized and unsynchronized objects
\end{itemize}
By pressing on the button saying "Reset Filter", the standard filter, which shows all \code{Worktime} objects that are not synchronized, will be set.
To apply the entered filter, the "Filter" button has to be pressed. This will first validate the entered values. This means that it checks if all fields are filled out and that the end date is set later than the start date. Then it will call the method \code{filterWorktimes()}, of the class\code{ListingProvider} which will return all the filtered \code{Worktime} objects, that will then be displayed.
\paragraph*{Add}\\
In case a user forgot to log time, we created the add function, so that the user is able to manually add an entry. A click on the button at the bottom right with the plus icon will open a dialog, which looks exactly like the edit dialog, just the title and the buttons are renamed. Also the \code{TextView} components have no standard text. After a click on the confirm button, a validation, that every field is filled out correctly, is being made. Using the entered values, a new \code{Worktime} object will be created and sent to the \code{Webservice}, which will send it to the worktime server.

\subsubsection{Using the Worktime List as a guest}\label{sec:guestWorktime}

When logged in as a guest, only locally saved \code{Worktime} objects will be displayed, which are the ones that are created when the user records time as a guest and can not select an issue. Also, it is not possible to synchronize the \code{Worktime} objects. These will be synchronized automatically after the user logs in with an account and assigns an issue to the recorded \code{Worktime} objects using the edit dialog.
\pagebreak