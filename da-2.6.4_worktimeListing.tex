%Worktime Listing

\renewcommand{\familydefault}{\sfdefault}
\Author{\daAuthorOne}

\label{Worktime Listing}
\subsection{Worktime Listing}
It is essential for the user to be able to see the times he or she already logged and hence it is important to present it to him or her in a format that is easy to comprehend and practical in usage.
Listing the \code{Worktime} objects does not only allow the user to visualise the time he or she was working but furthermore benefits the process of editing, adding and synchronizing \code{Worktime} entries. A structured and ordered list is required for us as the developers who have to access the \code{Worktime} objects as well as for the user for obvious reasons.
\paragraph*{Back-End Listing}
The class \code{ListingProvider} was implemented using the Singleton-Coding-Pattern to prevent the existence of multiple instances at any point during runtime. Various methods are provided that can be accessed and used to display \code{Worktime} objects as well as issues and projects. However, listing and filtering issues and projects will be described in the section \ref{Project/Issue Listing} \glqq\titleref{Project/Issue Listing}\grqq \ on page \pageref{Project/Issue Listing}.
\paragraph*{Initiating Worktime Objects}
Before any action can be taken, \code{set}-Methods in \code{ListingProvider} need to be called to initiate the \code{Worktime} objects the class should work with. This will be done when a \code{getWorktimes} request is performed.\\
The following method will be called after requesting a list of all available \code{Worktime} entries.
\begin{lstlisting}[caption={setWorktimes() Method in ListingProvider}]
public void setWorktimes(ArrayList<WorktimeRetrieved> worktimes){
    this.worktimes.clear();
    for (WorktimeRetrieved worktime_r : worktimes){
        this.worktimes.add(worktime_r.transformToWorktime());
    }
}
\end{lstlisting}
Requesting \code{Worktime} entries of a specific period of time will trigger the following method.
\begin{lstlisting}[caption={setWorktimesDateSpecific() Method in ListingProvider}]
public void setWorktimesDateSpecific(ArrayList<WorktimeRetrieved> worktimesDateSpecific){
    this.worktimesDateSpecific.clear();
    if(worktimesDateSpecific!=null && worktimesDateSpecific.size()>0){
        for (WorktimeRetrieved worktime_r : worktimesDateSpecific){
            this.worktimesDateSpecific.add( worktime_r.transformToWorktime());
        }
    }
}
\end{lstlisting}
Previously discussed methods are called after a request via the methods \linebreak\code{listAllWorktimes()} and \code{listWorktimesOfDay()} of the \code{Webservice} class. However, this will result in the lists only containing synchronized \code{Worktime} entries. \linebreak\code{Worktime} objects that are not yet synchronized and therefore stored locally will not be included yet. They can be added to the full list of \code{Worktime} objects using the \linebreak\code{addLocalWorktimes()} method as follows.
\begin{lstlisting}[caption={addLocalWorktimes() Method of ListingProvider}]
public void addLocalWorktimes(List<Worktime> localWorktimes){
    if(worktimes == null || worktimes.size() == 0){
        worktimes = new ArrayList<>(localWorktimes);
    }
    else{
        for (Worktime worktime : localWorktimes){
            if(!worktimes.contains(worktime)){
                worktimes.add(worktime);
            }
        }
    }
}
\end{lstlisting}
The different lists \code{worktimes} and \code{worktimesDateSpecific} will be used separately in order to ensure that only necessary \code{Worktime} objects will be processed. All lists are available for other classes via their \code{get}-Method.

\subsubsection{Filtering Worktimes}
Presenting all \code{Worktime} objects at all times would lead to a confusingly full list of \linebreak\code{Worktime} entries for the user. In order to avoid this scenario and also to provide functionality, \code{ListingProvider} is equipped with methods to filter and sort the existing \code{Worktime} objects. 
\\\\
Whenever a user refreshes the list of \code{Worktime} objects within their application, a basic filter will be applied. Within this filter, all \code{Worktime} objects from the past three days will be displayed and locally stored \code{Worktime} objects that are not yet synchronized with the server will be leading with the synchronized \code{Worktime} objects tailing. Both synchronized and unsynchronized \code{Worktime} objects are sorted by date. This order is achieved using the \code{Comparator} interface.
\pagebreak
\begin{lstlisting}[caption={sortWorktimesBasicRefresh()-Method for Default Sorting of Worktime Objects}]
public ArrayList<Worktime> sortWorktimesBasicRefresh(){
    ArrayList<Worktime> filteredWorktimes = new ArrayList<>();
    long day_in_ms = 1000*60*60*24;
    Date today = new Date(System.currentTimeMillis()); //today
    Date daysago = new Date(System.currentTimeMillis() - (3* day_in_ms)); //3 days ago
    for (Worktime worktime: worktimes){
        if(worktime.isSynced() && worktime.getStartDate().after(daysago) && worktime.getStartDate().before(today)
               || !worktime.isSynced()){
            filteredWorktimes.add(worktime);
        }
    }
    Comparator<Worktime> wtCompi = new Comparator<Worktime>(){
        @Override
        public int compare(Worktime wt1, Worktime wt2){
           int compval =  wt1.isSynced() && !wt2.isSynced()? 1 : !wt1.isSynced() && wt2.isSynced() ? -1 : 0;
           if(compval != 0){
               return compval;
           }
           compval = wt2.getEndDate().compareTo(wt1.getEndDate());
           return compval;
        }
    };
    Collections.sort(filteredWorktimes,wtCompi);
    return filteredWorktimes;
}
\end{lstlisting} 
When a user applies a filter to the list of \code{Worktime} objects, the following method within \code{ListingProvider} is called.
\begin{lstlisting}[caption={filterWorktimes() Method in ListingProvider}]
public ArrayList<Worktime> filterWorktimes(String projectkey, long startdatemillis, long enddatemillis, boolean synced){
    ArrayList<Worktime> filteredWorktimes = new ArrayList<>();
    if(projectkey.contains("-")){
        projectkey = projectkey.split("-")[0];
    }
    for (Worktime worktime: worktimes){
        boolean enddatecheck = false;
        if(worktime.getEndDate() == null || worktime.getEndDate().before(new Date(enddatemillis))){
            enddatecheck = true;
        }
        if (synced == worktime.isSynced() &&
                worktime.getStartDate().after(new Date(startdatemillis)) && enddatecheck){
            if (projectkey.equalsIgnoreCase("All Projects")){
                filteredWorktimes.add(worktime);
            }
            else if (worktime.getIssueKey().contains(projectkey)){
                filteredWorktimes.add(worktime);
            }
        }
    }
    return filteredWorktimes;
}
\end{lstlisting}
The method filters for all applied values and returns \code{Worktime} objects that fit all specified criteria.\\
When it is necessary to know all \code{Worktime} objects of a certain project, the following method will be called.
\begin{lstlisting}[caption={getWorktimesOfProject() Method in ListingProvider}]
public ArrayList<Worktime> getWorktimesOfProject(String projectkey, Worktime... worktimearray){
    ArrayList<Worktime> allWorktimesAvailable = null;
    if(worktimearray != null && worktimearray.length >= 0){
       allWorktimesAvailable = new ArrayList<>(Arrays.asList(worktimearray));
    }
    else{
       allWorktimesAvailable = new ArrayList<>(worktimes);
    }
    if(projectkey.equalsIgnoreCase("All projects")){
        return allWorktimesAvailable;
    }
    if(projectkey.contains("-")){
        projectkey = projectkey.split("-")[0];
    }
    ArrayList<Worktime> filteredWorktimes = new ArrayList<>();
    for(Worktime worktime : allWorktimesAvailable){
       if(worktime.getIssueKey().contains(projectkey)){
           filteredWorktimes.add(worktime);
       }
    }
    return filteredWorktimes;
}
\end{lstlisting}
 A projectkey as \code{String} is required to be passed into the method but also an optional \code{Array} of \code{Worktime} objects can be set as parameter. This array would be preferred before the already instantiated list \code{worktimes} if it was passed into the method. The method returns \code{Worktime} objects that are associated with the specified project only.
\\\\
These are all methods providing functionalities of filtering and sorting \code{Worktime} objects according to either default values or user specified ones.
\pagebreak