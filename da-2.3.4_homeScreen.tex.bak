%Start-Stop

\renewcommand{\familydefault}{\sfdefault}
\Author{\daAuthorFour}

\subsection{Home Screen}
The home screen is the place where you land after a successful login. This menu shows the button to start and stop the time recording, as well as a drop-down menu to choose a project, which has to be selected before starting the timer. 

\begin{figure}[h]
\includegraphics[width=0.3\textwidth]{images/HomeScreen_started.jpg}
\caption{Home screen with running timer}
\end{figure}

\subsubsection{The design of the home screen}

The design of the home screen is very basic, with the main element consisting of a \textit{ConstraintLayout}, which aligns two \textit{TextView} elements, one that shows the start time and one for the timer. As well as two \textit{MaterialButton} elements, one representing the dropdown menu and one is used to start the time recording. How the \textit{ConstraintLayout} works will be described in the chapter "Layouts and how designing in Android Studio works". Summarized the \textit{TextView} and \textit{MaterialButton} elements are constrained to their parent, which is in this case the border of the screen and have an fixed margin to the sides, top and bottom, which keep them in place.
\\


\subsubsection{Starting the time recording}

In order to start recording the time, the button "Start Timer" has to be pressed. After a click on the button, the \textit{onClickListener()} event will trigger. This will first check, if the user is logged in, or if he is using the app as a guest (the usage of the home screen as a guest is described below). Then it will check, if an issue is selected. If no issue is select and the user is logged in, the user will be notified with a \textit{Toast} to select an issue before starting. If an issue is selected the method \textit{startWork()} method will be called.

\begin{lstlisting}[caption={onClickListener for the timer button}]
btnTimer.setOnClickListener(new View.OnClickListener()
{
  @Override
  public void onClick(View view)
  {
    if(btnTimer.getText().equals("Start Timer"))
    {
      if((!mtbDropdown.getText().toString().equals("Select a project") && !webservice.loadUsername().equals("Guest")) || webservice.loadUsername().equals("Guest"))
      {
        startWork();
      }
      else
      {
        Toast.makeText(getApplicationContext(), "Issue not selected", Toast.LENGTH_LONG).show();
      }
    }
    else
    {
      stopWork();
    }
  }
});
\end{lstlisting}

\\
The \textit{startWork()} first sets the started time and the issue of the \textit{Worktime} object to the selected issue and then then adds it as unfinished work, to the local database. Finally, it will set the text of the timer button to "Stop Work" and call the \texit{startTimer()} method which is described on page \pageref{sec:timer}
\pagebreak

\subsubsection{Stoping the time recording}
When the timer is running, it can be stopped, by pressing the "Stop Timer" button. Following that, the unfinished \textit{Worktime} object, that was first stored in the database, will get the end date assigned. Now a dialog will pop up, where the user can enter a description and either confirm or cancel it. 

\begin{figure}[h]
\includegraphics[width=0.3\textwidth]{images/stop_dialog.jpg}
\caption{Stop dialog}
\end{figure}

When the confirm button is pressed, the saved \textit{Worktime} object will be send to the webservice which handles the rest. This will be described later.

\subsubsection{Timer}\label{sec:timer}

The timer \textit{TextView}, when started, displays the time the user is already working in the ISO 8601 format hh:mm:ss.

When the button "Start Timer" is pressed, it will call the method \textit{startWork()}, which was described before and furthermore will eventually call the method \textit{startTimer()}. This method creates a \textit{java.util.Timer} object, from which the method \textit{scheduleAtFixedRate()} is called and additionally a \textit{TimerTask} for the \textit{Timer} is set.\\ The advantage of using a \textit{Timer} with a \textit{TimerTask} is, that you can set a delay for the start and also set a period, which is the time the thread has to wait between each new run. \\The \textit{Thread}, that was used is a \textit{runOnUiThread()}. This type of \textit{Thread} is especially useful when something is changed on the UI. In our case, the \textit{TextView} which displays the timer is getting changed every second. The \textit{Thread} is has a period of one second, so that everytime the \textit{Thread} runs through again, one second has passed and the timer can be increased. Inside it the timer, the recorded seconds will be formatted. For example, the \textit{seconds} variable has a value of 10400, the timer will be formatted to 02:53:20. 
\\
\begin{lstlisting}[caption={Function for starting the timer}]
public void startTimer(Date startDate)
{
  mtbDropdown.setEnabled(false);
  mtbDropdown.getBackground().setAlpha(69);
  workTimer = new Timer();
  workTimer.scheduleAtFixedRate(new TimerTask()
  {
    @Override
    public void run()
    {
      runOnUiThread(new Runnable()
      {
        @Override
        public void run()
        {
          if(hours < 10 && minutes < 10 && seconds < 10)
            tvTimer.setText("0"+hours+":0"+minutes+":0"+seconds);
          if(hours < 10 && minutes < 10 && seconds > 10)
            tvTimer.setText("0"+hours+":0"+minutes+":"+seconds);
          if(hours < 10 && minutes > 10 && seconds > 10)
            tvTimer.setText("0"+hours+":"+minutes+":"+seconds);
          if(hours > 10 && minutes > 10 && seconds > 10)
            tvTimer.setText(hours+":"+minutes+":"+seconds);
          if(hours > 10 && minutes < 10 && seconds < 10)
            tvTimer.setText(hours+":0"+minutes+":0"+seconds);
          if(hours > 10 && minutes > 10 && seconds < 10)
            tvTimer.setText(hours+":"+minutes+":0"+seconds);
          if(hours > 10 && minutes < 10 && seconds > 10)
            tvTimer.setText(hours+":0"+minutes+":"+seconds);
          if(hours < 10 && minutes > 10 && seconds < 10)
            tvTimer.setText("0"+hours+":"+minutes+":0"+seconds);
          
          seconds++;
          if(seconds == 60)
          {
            minutes++;
            seconds = 0;
          }
          if(minutes == 60)
          {
            hours++;
            minutes = 0;
          }
        }
      });
    }
  }, 1000, 1000);
}
\end{lstlisting} 

\subsubsection{Multilevel Dropdown Menu}

In order to assign the recorded worktime to an issue, a multilevel dropdown menu, with the first level containing all projects and the second one showing all the correlating issues of the employee, was necessary. That way, a user can easily see and select all the active and selectable projects/issues. \\\\
At the time of the development, \textit{Material Design} did not support multilevel dropdown menus. The workaround to this, was to create a material design button, with an icon that made it look like a dropdown menu. After a click on the button, a \textit{androidx.appcompat.widget.PopupMenu} will be created, which will inflate a menu item for each project, that is assigned to the user.
\begin{figure}[H]
\includegraphics[width=0.3\textwidth]{images/dropdown_level1.jpg}
\caption{Dropdown Level 1}
\end{figure}
\\
After a second click on a project item, the webservice will be called and the issues of the project which are assigned to the user, will be returned and displayed again as dropdown menu.
\\
\begin{figure}[H]
\includegraphics[width=0.3\textwidth]{images/dropdown_level2.jpg}
\caption{Dropdown Level 2}
\end{figure}

\subsubsection{Using the home menu as a guest}

When the user has no internet connection or some sort of connection problem, the guest login may be used. The limitations of the guest user are, that there are no selectable projects or issues. Still, the time recording can be started, but no issue will be assigned. After login in with a valid user again, the issue can be assigned in the \textit{Worktime List}, which will be explained later on.


\pagebreak