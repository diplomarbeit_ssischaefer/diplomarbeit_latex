\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {1}ImageView for profile picture}{39}{lstlisting.1}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {2}Menu list item}{39}{lstlisting.2}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {3}NavigationView with menu\_user.xml as header and menu\_list.xml as menu}{40}{lstlisting.3}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {4}Method called after pressing the back button}{40}{lstlisting.4}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {5}Method called when navigating between functionalities}{41}{lstlisting.5}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {6}Transitions for switching between activities}{41}{lstlisting.6}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {7}Text-Input for login with imeOptions}{43}{lstlisting.7}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {8}EditorActionListener to change focus of input field}{43}{lstlisting.8}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {9}Listener for Remember-Me option}{44}{lstlisting.9}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {10}Loading setting of last session}{44}{lstlisting.10}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {11}Handling the user's log-in}{45}{lstlisting.11}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {12}onClickListener() for the timer button}{47}{lstlisting.12}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {13}Method for starting the timer}{49}{lstlisting.13}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {14}onRefreshListener() for the SwipeRefreshLayout}{52}{lstlisting.14}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {15}RecyclerView.Adapter}{54}{lstlisting.15}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {16}ViewHolder}{54}{lstlisting.16}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {17}ItemCLickListener}{54}{lstlisting.17}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {18}Dialog}{55}{lstlisting.18}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {19}Synchronize}{56}{lstlisting.19}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {20}RecyclerView with an adapter for each project item}{58}{lstlisting.20}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {21}Adding projects with the help of the adapter class}{59}{lstlisting.21}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {22}Adding projects to adapter}{59}{lstlisting.22}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {23}Displaying the color of each project}{60}{lstlisting.23}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {24}Setting the color of a project}{60}{lstlisting.24}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {25}Opening a dialog for picking a color of the project}{61}{lstlisting.25}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {26}getItem()}{63}{lstlisting.26}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {27}Validating the e-mail address}{64}{lstlisting.27}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {28}Opening a dialog to select a specific date}{65}{lstlisting.28}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {29}Checking for auto-log-in setting of previous session}{67}{lstlisting.29}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {30}Selecting a time for when the notification will be sent}{67}{lstlisting.30}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {31}Click-Listener for default issue}{68}{lstlisting.31}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {32}Drop-down of available projects}{68}{lstlisting.32}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {33}Drop-down of available issues of selected project}{69}{lstlisting.33}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {34}Generating Retrofit Object}{72}{lstlisting.34}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {35}POJO Class example with one variable}{73}{lstlisting.35}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {36}Singleton Pattern}{74}{lstlisting.36}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {37}Communication Methods for Database and REST API Operations}{75}{lstlisting.37}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {38}Retrofit2 Call Interface}{78}{lstlisting.38}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {39}AsyncTask Abstract Class}{78}{lstlisting.39}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {40}Retrofit Annotations Content Types}{80}{lstlisting.40}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {41}Retrofit Annotations GET and POST Parameter}{80}{lstlisting.41}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {42}Retrofit Basic POST and GET Request}{81}{lstlisting.42}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {43}Retrofit Login Request Method}{82}{lstlisting.43}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {44}Login Response}{82}{lstlisting.44}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {45}Callback Object for Login}{83}{lstlisting.45}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {46}Login failed notification}{83}{lstlisting.46}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {47}Retrofit CheckToken Request Method}{84}{lstlisting.47}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {48}CheckToken Response for expired token}{84}{lstlisting.48}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {49}If-Else Statement for token validation}{85}{lstlisting.49}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {50}ValidationComplete() Method in LoadingActivity.java}{85}{lstlisting.50}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {51}Retrofit getWorktime Request Method}{86}{lstlisting.51}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {52}getWorktime Response}{87}{lstlisting.52}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {53}Callback Object for getWorktime}{87}{lstlisting.53}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {54}Calling the getWorktime Request}{88}{lstlisting.54}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {55}Worktime Format for request}{89}{lstlisting.55}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {56}Retrofit PostWorktime Request Method}{90}{lstlisting.56}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {57}PostWorktime Response}{90}{lstlisting.57}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {58}GetPreparedDataForRequest() Method in Callback Object of PostWorktime}{91}{lstlisting.58}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {59}Callback Object for PostWorktime}{91}{lstlisting.59}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {60}Retrofit Delete Request Method}{92}{lstlisting.60}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {61}DeleteWorktime Response}{92}{lstlisting.61}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {62}If-Else Statement for deleting entries}{93}{lstlisting.62}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {63}getIssueInfo Response}{94}{lstlisting.63}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {64}Basic Authentication Interceptor for Jira Login}{95}{lstlisting.64}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {65}Receiving Cookies Interceptor for Jira Communication }{96}{lstlisting.65}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {66}Storing Cookies - Methods.setCookies()}{96}{lstlisting.66}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {67}Building the OkHttpClient}{97}{lstlisting.67}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {68}Jira Login Retrofit Interface Method}{97}{lstlisting.68}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {69}Callback Object for Jira Login}{98}{lstlisting.69}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {70}Jira getIssues Response}{100}{lstlisting.70}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {71}Retrofit Interface Method for Requesting Issues via Jira}{100}{lstlisting.71}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {72}Callback Object for Requesting Issues (getIssues)}{101}{lstlisting.72}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {73}Adding Cookies Interceptor for Jira Communication}{102}{lstlisting.73}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {74}Jira Logout Retrofit Interface Method}{102}{lstlisting.74}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {75}Jira Logout Callback Object}{103}{lstlisting.75}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {76}Removing Cookies - Methods.removeCookies()}{103}{lstlisting.76}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {77}Room Database Class}{105}{lstlisting.77}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {78}DAO Method getAll()}{106}{lstlisting.78}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {79}DAO Method insertWorktime()}{106}{lstlisting.79}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {80}DAO Method deleteAll()}{106}{lstlisting.80}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {81}DAO Method getRunningWorktime()}{106}{lstlisting.81}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {82}DAO Method updateOnGoingWorktime()}{106}{lstlisting.82}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {83}DAO Method deleteWorktime()}{106}{lstlisting.83}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {84}Worktime Room Entity Annotations}{107}{lstlisting.84}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {85}Webservice Method loadWorktimesFormDatabase()}{108}{lstlisting.85}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {86}AsyncTask for Read Database}{109}{lstlisting.86}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {87}Webservice Method deleteAllWorktimesFromDatabase()}{109}{lstlisting.87}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {88}AsyncTask for Clear Database}{110}{lstlisting.88}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {89}Webservice Method saveWorktimeInDatabase()}{110}{lstlisting.89}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {90}AsyncTask for storing Worktimes in Database}{111}{lstlisting.90}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {91}Webservice Method stopWorktime()}{112}{lstlisting.91}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {92}Webservice Method editWorktime()}{112}{lstlisting.92}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {93}AsyncTask for updating Worktimes in database}{113}{lstlisting.93}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {94}widget\_info.xml}{116}{lstlisting.94}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {95}Create Widget Instance}{117}{lstlisting.95}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {96}Check for multiple App Widget Instances}{118}{lstlisting.96}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {97}WidgetActivity onUpdate}{121}{lstlisting.97}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {98}WidgetActivity performConfig()}{122}{lstlisting.98}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {99}WidgetActivity onDisabled()}{122}{lstlisting.99}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {100}WidgetActivity onReceive()}{123}{lstlisting.100}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {101}WidgetActivity isOverOneMinute()}{124}{lstlisting.101}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {102}WidgetActivity startWork()}{125}{lstlisting.102}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {103}WidgetActivity getPendingSelfIntent()}{125}{lstlisting.103}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {104}CustomBroadcastReceiver onReceive()}{126}{lstlisting.104}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {105}CustomBroadcastReceiver getCurrentTime()}{126}{lstlisting.105}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {106}Statistics Weekly onViewCreated()}{128}{lstlisting.106}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {107}Statistics Weekly onUpdate()}{129}{lstlisting.107}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {108}Statistics Weekly hoursPerProject()}{130}{lstlisting.108}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {109}export onExportTime()}{132}{lstlisting.109}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {110}setWorktimes() Method in ListingProvider}{134}{lstlisting.110}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {111}setWorktimesDateSpecific() Method in ListingProvider}{134}{lstlisting.111}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {112}addLocalWorktimes() Method of ListingProvider}{135}{lstlisting.112}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {113}sortWorktimesBasicRefresh()-Method for Default Sorting of Worktime Objects}{136}{lstlisting.113}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {114}filterWorktimes() Method in ListingProvider}{136}{lstlisting.114}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {115}getWorktimesOfProject() Method in ListingProvider}{137}{lstlisting.115}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {116}Initiating Projects and Issues in ListingProvider}{138}{lstlisting.116}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {117}getIssuesOfProjects() Method in ListingProvider}{139}{lstlisting.117}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {118}Sorting Issues Alphabetically}{139}{lstlisting.118}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {119}Cloud Messaging Manifest Entry}{141}{lstlisting.119}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {120}scheduleNotification()}{141}{lstlisting.120}%
\defcounter {refsection}{0}\relax 
\contentsline {lstlisting}{\numberline {121}onMessageReceived()}{144}{lstlisting.121}%
