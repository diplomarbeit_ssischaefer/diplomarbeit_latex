%Login Jira

\renewcommand{\familydefault}{\sfdefault}
\Author{\daAuthorOne}

\subsubsection{Login Jira}
Just as we need the connection with the worktime-server, we need to connect with the Jira-REST-API in order for the mobile application to work as desired. To establish this connection, a user has to be authenticated. After a successful authentication using Basic Authentication as recommended a Cookie will be received. This Cookie works just as the token with the worktime-server and needs to be set for every further request.
\vspace{-3mm}
\paragraph*{Properties}
URL: \url{https://jira.ssi-schaefer.com/rest/auth/1/session}\\
Request-Method: \code{POST}\\
Content-Type: \code{application/json}
\vspace{-3mm}
\paragraph*{Parameters}
\begin{itemize}
\item username \code{<String>}
\vspace{-2mm}
\item password \code{<String>}
\end{itemize}
\paragraph*{Basic Authentication}
Basic Authentication adds an authentication header to the request containing the credentials of the user. Since there will be multiple requests sent to the Jira-REST-API, it is applicable to use an \code{Interceptor} to add the necessary values to the request.
Using \code{Credentials} an authentication string for the Basic Authentication scheme is created that is then inserted into the request header.
\begin{lstlisting}[caption={[Basic Authentication Interceptor for Jira Login]{Basic Authentication Interceptor for Jira Login \parencite{basicauthcode}}}]
public class BasicAuthInterceptor implements Interceptor {
    private String credentials;
    public BasicAuthInterceptor(String username, String password) {
        this.credentials = Credentials.basic(username,password);
    }
    
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request authenticatedRequest = request.newBuilder().header("Authorization", credentials).build();
        return chain.proceed(authenticatedRequest);
    }
}
\end{lstlisting}
\paragraph*{Cookie Handling}
Apart from the \code{BasicAuthInterceptor}, a \code{ReceivedCookiesInterceptor} is necessary for this request due to the fact that the response will contain a Cookie with the \linebreak\code{JSESSIONID} and the information contained within the Cookie need to be stored for any requests that will be made in the further process.\\
When a response contains a Cookie with the \code{JSESSIONID}, this Cookie will be stored locally. This functionality can be found in the static method \code{Methods.setCookies()} which requires the context of the application that was passed to the constructor of the interceptor.
\begin{lstlisting}[caption={[Receiving Cookies Interceptor for Jira Communication ]{Receiving Cookies Interceptor for Jira Communication \parencite{cookieinterceptorcodes}}}, label={ReceivedCookiesInterceptor}]
public class ReceivedCookiesInterceptor implements Interceptor{
    private Context mContext;
    public ReceivedCookiesInterceptor(Context mContext){
        this.mContext = mContext;
    }
    
    @Override
    public Response intercept(Chain chain) throws IOException{
        Response originalResponse = chain.proceed(chain.request());
        if(!originalResponse.headers("Set-Cookie").isEmpty()){
            HashSet<String> cookies = new HashSet<>();
            for (String header : originalResponse.headers("Set-Cookie")){
                if(header.contains("JSESSIONID")){
                    cookies.add(header);
                }
            }
            Methods.setCookies(mContext,cookies);
        }
        return originalResponse;
    }
}
\end{lstlisting}
\begin{lstlisting}[caption={Storing Cookies - Methods.setCookies()}]
public class Methods{
    private static final String SHARED_PREFS = "com.ssi.zeiterfassungsapp";
   ...
    public static boolean setCookies(Context mContext, HashSet<String> cookies){
        SharedPreferences myPreferences = mContext.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreferences.edit();
        return editor.putStringSet("cookies",cookies).commit();
    }
}
\end{lstlisting}
\newpage
\paragraph*{Intercepting Request and Response}
These interceptors are added to the \code{OkHttpClient} that is used for the connection with Jira. Upon request, the \code{BasicAuthInterceptor} will add necessary authentication information to the request header and the \code{ReceivedCookiesInterceptor} will store the Cookie that will be included in the response. After adding the interceptors to the \linebreak\code{OkHttpClient}, these actions will be performed without an explicit call.
\paragraph*{OkHttpClient}
The simple and efficient \code{OkHttpClient} works as a requester for the Jira connection. It is updated according to the needs of the specific request. If it is a login request as described in this section, the previously discussed interceptors are added. In other cases, it is necessary to use the \code{AddCookieInterceptor} together with the \code{ReceivedCookiesInterceptor} or only one interceptor. The interceptors needed are parsed into the method that builds and returns the \code{OkHttpClient} which is then used for providing the Retrofit service.
\begin{lstlisting}[caption={Building the OkHttpClient}, label={Build OkHttpClient}]
private OkHttpClient getOkHttpClient(Interceptor... interceptors) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60 * 5, TimeUnit.SECONDS)
                .readTimeout(60 * 5, TimeUnit.SECONDS)
                .writeTimeout(60 * 5, TimeUnit.SECONDS);
        for (Interceptor interceptor : interceptors) {
            okHttpClient.interceptors().add(interceptor);
        }
        return okHttpClient.build();
    }
\end{lstlisting}
\vspace{-6mm}
\paragraph*{Retrofit Interface Method}
The Retrofit interface method is called upon requesting login with Jira. The Basic Authentication header will be added by the cohering interceptor.  
\begin{lstlisting}[caption={Jira Login Retrofit Interface Method}]
@FormUrlEncoded
@POST("auth/1/session")
@Headers("Content-Type: application/json")
Call<ResponseBody> authenticateJira(@Field("username") String username, @Field("password") String password);
\end{lstlisting}
\vspace{-6mm}
\paragraph*{Successful Response}
After a successful response with the code \code{200}, all functionalities of Jira are available to the user since the Cookies were stored accurately after the user authenticated successfully using his or her credentials.
\paragraph*{Error Responses}
There are two major error response codes.
\begin{itemize}
\item \code{401 - Unauthorized} occurs when the credentials are invalid.
\item \code{403 - Forbidden} occurs when the authentication is currently denied but the credentials might still be valid. 
\end{itemize}
Upon a \code{401} response code, the user will be prompted to re-enter his or her credentials again.
Since the reason for temporarily denied authorisation (code \code{403}) is usually a CAPTCHA challenge, whenever this error occurs, the user has to log into the web-interface of Jira. Until access is granted again, no functionalities provided through the connection with Jira will be available to the user. This includes project and issue listing.
\paragraph*{Callback Object}
With the following object the response is processed after the \code{Interceptor} stored the Cookie accurately. As long as no error code was received and the Cookie was stored, which is checked using \code{Methods.isCookieSet()}, the authentication was successful and the appropriate method of the \code{AsyncDelegate} is called. The user can thereafter use all functionalities the connection with the Jira-REST-API provides.
\begin{lstlisting}[caption={Callback Object for Jira Login}]
public class JiraAuthenticator implements Callback {
   ...
    @Override
    public void onResponse(Call call, Response response) {
        if(response.code() != 401 && response.code() != 403) {
            if(Methods.isCookieSet(mContext)){
                delegate.authenticationJiraComplete(true);
            }
        }
        else {
            delegate.authenticationJiraComplete(false);
        }
    }
}
\end{lstlisting}
\newpage