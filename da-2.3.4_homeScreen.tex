%Start-Stop

\renewcommand{\familydefault}{\sfdefault}
\Author{\daAuthorFour}

\subsection{Home Screen}
The home screen is the place where the user will land after a successful login. This menu shows the button that starts and stops the time recording, as well as a drop-down menu to choose an issue of a project, which has to be selected before starting the timer. 

\begin{figure}[h]
\includegraphics[width=0.3\textwidth]{images/HomeScreen_started.jpg}
\caption{Home screen with running timer}
\end{figure}

\subsubsection{The design of the home screen}

The design of the home screen is very basic with the main components consisting of a \linebreak\code{ConstraintLayout}, which aligns two \code{TextView} elements, one that shows the start time and one for the timer. As well as two \code{MaterialButton} components, one representing the dropdown menu and one used to start or stop the time recording. \\How the \code{ConstraintLayout} works will be described in the section \ref{sec:layouts} "Layouts used in our application" on page \pageref{sec:layouts}. Summarized, the \code{TextView} and \code{MaterialButton} components are constrained to their parent which is in this case the border of the screen. They have fixed margins to the sides, top and bottom, which keep them in place.
\\


\subsubsection{Starting the time recording}\label{sec:start}

In order to start recording the time, the start button has to be pressed to trigger the\linebreak\code{onClickListener()} event. This will first check if the user is logged in or if he or she is using the application as a guest (the usage of the home screen as a guest is described on page \pageref{sec:guesthome}). Then it will check if an issue is selected. If no issue is selected and the user is logged in, the user will be notified with a \code{Toast} to select an issue before starting. If an issue is selected, the method \code{startWork()} will be called.

\begin{lstlisting}[caption={onClickListener() for the timer button}]
btnTimer.setOnClickListener(new View.OnClickListener(){
  @Override
  public void onClick(View view){
    if(btnTimer.getText().equals("Start Timer")){
      if((!mtbDropdown.getText().toString().equals("Select a project") && !webservice.loadUsername().equals("Guest")) || webservice.loadUsername().equals("Guest")){
        startWork();
      }
      else{
        Toast.makeText(getApplicationContext(), "Issue not selected", Toast.LENGTH_LONG).show();
      }
    }
    else{
      stopWork();
    }
  }
});
\end{lstlisting}

\\
The \code{startWork()} method first sets the started time and the issue of the \code{Worktime} object to the selected issue and then then adds it as unfinished work to the local database. Finally, it will set the text of the timer button to "Stop Work" and call the \code{startTimer()} method which is described on page \pageref{sec:timer} in the section "\ref{sec:timer} Timer".


\subsubsection{Stopping the time recording}
When the timer is running it can be stopped by pressing the stop button. Following that, the unfinished \code{Worktime} object that was first stored in the database will get the end date assigned. Now a dialog will pop up, where the user can enter a description and either confirm or cancel storing the \code{Worktime} object. 

\begin{figure}[h]
\includegraphics[width=0.3\textwidth]{images/stop_dialog.jpg}
\caption{Stop dialog}
\end{figure}

When the confirm button is pressed, the saved \code{Worktime} object will be sent to the \linebreak\code{Webservice} class which handles the rest.

\subsubsection{Timer}\label{sec:timer}

The \code{TextView} for the timer, when the timer has been started before, displays the time the user is already working in the ISO 8601 format \code{hh:mm:ss}.

When the start button is pressed, it will call the method \code{startWork()}, which was described in section \ref{sec:start}, and furthermore, will eventually call the method \code{startTimer()}. This method creates a \code{java.util.Timer} object from which the method\linebreak\code{scheduleAtFixedRate()} is called and additionally, a \code{TimerTask} for the \code{Timer} object is set. The advantage of using a \code{Timer} object with a \code{TimerTask} is that you can set a delay for the start and also set a period, which is the time a \code{Thread} has to wait between each new run. \\
The \code{Thread} that was used is a \code{runOnUiThread()}. This type of \code{Thread} is especially useful when something is changed on the UI. In our case, the \code{TextView}, which displays the timer, is being changed every second. The \code{Thread} has a period of one second, so that every time the \code{Thread} runs through again, one second has passed and the timer can be increased by one. Inside the  \code{runOnUiThread()}, the recorded seconds will be formatted. For example, if the \code{seconds} variable has a value of 10400, the timer will be formatted to 02:53:20. 
\pagebreak
\begin{lstlisting}[caption={Method for starting the timer}]
public void startTimer(Date startDate){
  mtbDropdown.setEnabled(false);
  mtbDropdown.getBackground().setAlpha(69);
  workTimer = new Timer();
  workTimer.scheduleAtFixedRate(new TimerTask(){
    @Override
    public void run(){
      runOnUiThread(new Runnable(){
        @Override
        public void run(){
          if(hours < 10 && minutes < 10 && seconds < 10)
            tvTimer.setText("0"+hours+":0"+minutes+":0"+seconds);
          if(hours < 10 && minutes < 10 && seconds > 10)
            tvTimer.setText("0"+hours+":0"+minutes+":"+seconds);
          if(hours < 10 && minutes > 10 && seconds > 10)
            tvTimer.setText("0"+hours+":"+minutes+":"+seconds);
          if(hours > 10 && minutes > 10 && seconds > 10)
            tvTimer.setText(hours+":"+minutes+":"+seconds);
          if(hours > 10 && minutes < 10 && seconds < 10)
            tvTimer.setText(hours+":0"+minutes+":0"+seconds);
          if(hours > 10 && minutes > 10 && seconds < 10)
            tvTimer.setText(hours+":"+minutes+":0"+seconds);
          if(hours > 10 && minutes < 10 && seconds > 10)
            tvTimer.setText(hours+":0"+minutes+":"+seconds);
          if(hours < 10 && minutes > 10 && seconds < 10)
            tvTimer.setText("0"+hours+":"+minutes+":0"+seconds);
          
          seconds++;
          if(seconds == 60){
            minutes++;
            seconds = 0;
          }
          if(minutes == 60){
            hours++;
            minutes = 0;
          }
        }
      });
    }
  }, 1000, 1000);
}
\end{lstlisting} 
\vspace{-6mm}
\subsubsection{Multilevel Dropdown Menu}

In order to assign the recorded \code{Worktime} object to an issue, a multilevel dropdown menu with the first level containing all projects and the second one showing all the correlating issues of the employee, was necessary. That way, a user can easily see and select all the active and selectable projects/issues. \\\\\vspace{-1mm}
At the time of the development, Material Design did not support multilevel dropdown menus. The workaround to this was to create a material design button with an icon that made it look like a dropdown menu. After a click on the button, a \code{androidx.appcompat.widget}\linebreak\code{.PopupMenu} will be created, which will inflate a menu item for each project that is assigned to the user.
\begin{figure}[H]
\centering
\captionsetup{justification=centering}
\includegraphics[width=0.3\textwidth]{images/dropdown_level1.jpg}
\caption{Dropdown Level 1}
\end{figure}
\\
After a click on a project item, the issues of the project which are assigned to the user will be returned and displayed again as dropdown menu.
\\
\begin{figure}[H]
\centering
\captionsetup{justification=centering}
\includegraphics[width=0.3\textwidth]{images/dropdown_level2.jpg}
\caption{Dropdown Level 2}
\end{figure}

\subsubsection{Using the home menu as a guest} \label{sec:guesthome}

When the user has no Internet connection, some sort of connection problem or does not have an account, the guest login may be used. The limitations of the guest user are that there are no selectable projects or issues. Still, the time recording can be started, but no issue will be assigned. After logging in with a valid user again, the issue can be assigned in the UI of the \code{WorktimeActivity}.

\pagebreak