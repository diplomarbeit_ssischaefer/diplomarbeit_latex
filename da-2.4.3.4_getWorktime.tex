%GetWorktime

\renewcommand{\familydefault}{\sfdefault}
\Author{\daAuthorOne}

\subsubsection{GetWorktime}
Before a user can see the times he or she already worked together with the associated issue, it is necessary to load them. When the \code{Worktime} objects are already synchronized with the server, the \code{getWorktime} request is used to retrieve them in order to present them to the user.
\vspace{-3mm}
\paragraph*{Properties}
URL: \url{https://worktime.wamas.com/api/getWorktime/}\\
Request Method: \code{GET}
\vspace{-3mm}
\paragraph*{Parameters}
\begin{itemize}
\item tokenid \code{<String>}
\vspace{-2mm}
\item datefrom (optional) \code{<YYYYMMDD>}
\vspace{-2mm}
\item dateto (optional) \code{<YYYYMMDD>}
\vspace{-2mm}
\item bookuser (optional) \code{<String>}
\end{itemize}
\vspace{-5mm}
\paragraph*{Retrofit Interface Method}
Any number of parameters can be added in the request and it is the responsibility of the calling method to provide the accurate format for the parameters. To store the response accurately, the POJO class \code{WorktimeResponse} was created.
\begin{lstlisting}[caption={Retrofit getWorktime Request Method}]
@GET("getWorktime")
Call<WorktimeResponse> requestWorktimes(@QueryMap Map<String,String> params);
\end{lstlisting}
\pagebreak
\paragraph*{Response}
A successful request would return a result similar to the example below in JSON format.
\begin{lstlisting}[caption={getWorktime Response}]
{
    "status": "Ok",
    "token": "XIXAX:f3e848d8c0796c1c1a88d65f464d43de",
    "user": "kerstin.klaminger@ssi-schaefer.com",
    "origin": "[NEW] WAMAS.COM",
    "target": "IF",
    "result": [
        {
            "BOOKINGID": "2030786",
            "ISSUEKEY": "SSIZEITERF1-25",
            "CREATEDBY": "kerstin.klaminger@ssi-schaefer.com",
            "CREATEDDATET": "16.12.2019 14:13:08",
            "MODIFIEDBY": null,
            "MODIFIEDDATET": null,
            "USERID": "kerstin.klaminger@ssi-schaefer.com",
            "STARTDATET": "11.12.2019 12:41:00",
            "ENDDATET": "11.12.2019 16:02:00",
            "BOOKINGTEXT": "Bookingtext01",
            ...
        }
    ]
}
\end{lstlisting}
\vspace{-6mm}
\paragraph*{Callback Object}\\
This class implementing the \code{Callback} interface processes the JSON-formatted response.
\begin{lstlisting}[caption={Callback Object for getWorktime}]
public class WorktimeLister implements Callback {
   ...
    @Override
    public void onResponse(Call call, Response response){
        Type type = new TypeToken<WorktimeResponse>(){}.getType();
        WorktimeResponse worktimeResponse = null; 
        Gson gson = new Gson();
        if(response.code() == 200){
            worktimeResponse = (WorktimeResponse) response.body();
            ListingProvider.getInstance().setWorktimes( worktimeResponse.getResult());
            delegate.worktimeLoadingComplete(true);
        }
        else{
            try{
                worktimeResponse = gson.fromJson(response.errorBody().string(), type);
            }
            catch(IOException e){
                Log.e(TAG,"Error: " + e.getMessage());
            }
            finally{
                delegate.worktimeLoadingComplete(false); 
            }
        }
    }
   ...
}
\end{lstlisting}
After a response is received, the method checks whether it was a successful one which is the case when the code of the response equals \code{200}. It will then transform the response into an object of the class \code{WorktimeResponse}. From there, it will retrieve the list of \code{WorktimeRetrieved} objects that are then set as a list within the \code{ListingProvider} instance. This instance will provide listing and filtering features for the objects.\\
After this procedure is completed, the \code{AsyncDelegate} will get notified about a successful completion. In case the response's code is not equal to \code{200}, the \code{AsyncDelegate} receives the notification that the request failed and will proceed accordingly.
\paragraph*{Usage}
The request is used to retrieve \code{Worktime} objects with specific dates or a complete list of all available worktime entries at the server. It is therefore called indirectly via the \code{Webservice} instance whenever one of these lists is needed.
These lists are being made accessible via the \code{ListingProvider} instance that lists and filters all types of objects that are displayed to the user. 
This procedure will be explained in the section \ref{Worktime Listing} \glqq\titleref{Worktime Listing}\grqq \ on page \pageref{Worktime Listing}.
\begin{lstlisting}[caption={Calling the getWorktime Request}]
public class Webservice
{
   ...
     private static final SimpleDateFormat dateformatRequestWorktimes = new SimpleDateFormat("yyyyMMdd");
   ...
     public void listAllWorktimes(){
        HashMap<String,String> params = new HashMap<>();
        params.put("tokenid",loadToken());
        Call call = worktimeService.requestWorktimes(params);
        call.enqueue(worktimeListingTask);
      }
     public void listWorktimesOfDay(Date startdate, Date enddate){
        HashMap<String,String> params = new HashMap<>();
        params.put("tokenid",loadToken());
        params.put("datefrom", dateformatRequestWorktimes.format(startdate));
        params.put("dateto", dateformatRequestWorktimes.format(enddate));
        Call call = worktimeService.requestWorktimes(params);
        call.enqueue(worktimeDayListingTask);
      }
   ...
}
\end{lstlisting}
As visible, the parameters are formatted in the correct way. The \code{token} will be added using the \code{loadToken()} method of the very same class while the \code{startdate} and \code{enddate} are formatted using the \code{SimpleDateFormat} that is stored as an instance variable with the name\code{dateformatRequestWorktimes}. All this is necessary to guarantee a constant and correct request at all times.
\pagebreak